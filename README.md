# Partie technique de la "Revue de Presse" de l'[![April](https://www.april.org/sites/default/themes/zen_april/logo.png)](https://www.april.org) 

Ici est géré et développé la présentation du site web, la mécanique `html` et `css`.

La base technique est [hugo](https://gohugo.io), un générateur de site web statique.

Il y a ensuite un thème nomme [ananka](https://github.com/budparr/gohugo-theme-ananke), surchargé par un thème spécifique nommé "fils".

Il est possible de personnaliser un peu plus le thème, notamment à l'aide de [`static/css/custom.css`](https://framagit.org/fils-du-net/april/fils/blob/master/static/css/custom.css).

## Comment démarrer

Avec les commandes:
* récupérer le code:`git clone --recurse-submodules git@framagit.org:fils-du-net/april/fils.git`
* aller dans le répertoire récupéré: `cd fils`
* lancer hugo: `hugo server`
* consulter le site local: `firefox http://localhost:1313`

Le nombre d'articles étant assez conséquent, le lancement prend quelques dizaines de secondes.

Vous pouvez ensuite créer, modifier, supprimer des articles, dans le répertoire `content/article`, au format "markdown". Puis recharger la page dans le navigateur.

En production, vous pouvez générer le site web avec la commande `hugo`, sans argument, qui génère un répertoire `public`.