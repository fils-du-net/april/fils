#!/bin/bash

mkdir /tmp/drupal2fils
from=0
to=2

declare -A months
months=(
	[janvier]='01'
	[février]='02'
	[mars]='03'
	[avril]='04'
	[mai]='05'
	[juin]='06'
	[juillet]='07'
	[août]='08'
	[septembre]='09'
	[octobre]='10'
	[novembre]='11'
	[nov]='11'
	[décembre]='12'
)

for j in $( seq $from $to )
do
	wget -c "https://www.april.org/revue-de-presse?page=$j" -P /tmp/drupal2fils

	rm -- *.xml

	xmllint --html --format --xmlout "/tmp/drupal2fils/revue-de-presse?page=$j" | xml_split -c "//div[@class='node-inner']"

	rm -- *-00.xml

	for i in *.xml
	do
		data=''
		site=''
		title=''
		author=''
		date=''
		href=''
		tags=''
		quote=''

		# xml_grep "div[@class='revue-de-presse']/p" $i
		site=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/p/strong[1]" "$i" | sed 's/&amp;/\&/g' )
		[[ $site == *"-"* ]] || [[ $site == *":"* ]] || [[ $site == *"@"* ]] && site="\"$site\""
		title=$( xmlstarlet sel -t -v "//div[@class='node-inner']/h2/a" "$i" | sed 's/\[.*\] //' | sed 's/"/\\"/g' )
		author=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/p/strong[2]" "$i" )
		date=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/p/strong[3]/@title" "$i" | sed 's/T.*//' )

		date=${date%% à *}

		if [ "$date" = '' ]
		then
			date=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/p/strong[3]" "$i" | tr '[:upper:]' '[:lower:]' )
			[ "$date" = '' ] && continue
			date=$( echo "$date" | grep -o '[[:digit:]].*' )
			date=${date//\//-}
			date=${date//\./-}
			date=${date%%[[:space:]]}
			date=${date%% à *}

			if [[ $date == *" "* ]]
			then
				month=$( echo "$date" | grep -o ' .* ' | sed 's/\ //g' )
				month=${months[$month]}
				jour=${date%% *}
				jour=${jour#0}
				# echo "Date: '$date'"
				# echo "Month: $month"
				# echo "Jour: $jour"
				date=${date##* }-$month-$( printf %02d "$jour" )
			else
				month=${date#*-}
				month=${month%-*}
				jour=${date%%-*}
				jour=${jour#0}
				date=${date##*-}-$month-$( printf %02d "$jour" )
			fi
		fi

		if [ "$date" = '' ]
		then
			data=$(grep -Eo "Extrait de l'article du site .* par .* en date du .*[[:digit:]]" "$i")
			[ "$date" = '' ] && continue

			site=${data#Extrait de l?article du site }
			site=${site% par * en date du *}
			author=${data#Extrait de l?article du site * par }
			author=${author% en date du*}
			date=${data#*en date du }
			date=${date% :*}
			month=${date#* }
			month=${month% *}

			if [ "$month" != "$date" ]
			then
				month=${months[$month]}
				jour=${date%% *}
				jour=${jour#0}
				date=${date##* }-$month-$( printf %02d "$jour" )
			else
				date=${date//\//-}
				date=${date//\./-}
				month=${date#*-}
				month=${month%-*}
				jour=${date%%-*}
				jour=${jour#0}
				date=${date##*-}-$month-$( printf %02d "$jour" )
			fi
			quote=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/p/em" "$i" | sed 's/^« //' | sed 's/ »$//' | sed 's/^/> /g' )
		fi

		# On poursuit les traitements
		[ "$date" = '' ] && echo 'Sortie car aucune date de publication' && continue

		annee=${date%%-*}
		[ "$annee" != '' ] && [ ${#annee} -lt 4 ] && annee="20$annee" && date="20$date"

		if [ "$href" = '' ]
		then
			href=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/p/a" "$i" | sed 's/&amp;/\&/g')
		fi
		tags=$( xmlstarlet sel -t -m "//div[@class='node-inner']//li[@class!='taxonomy_term_1586 first']/a[@rel='tag']" -o '
- ' -v 'text()' "$i" )

		if [ "$quote" = '' ]
		then
			quote=$( xmlstarlet sel -t -v "//div[@class='revue-de-presse']/blockquote/p/em" "$i" | sed 's/^« //' | sed 's/ »$//' | sed 's/^/> /g' )
		fi

		filename="content/article/$annee/"$( echo "$date" | sed 's/^....//' | sed 's/-//g' )-$( xmllint --xpath "string(//div[@class='node-inner']/h2/a/@href)" "$i" | sed 's/\///' )".md"

		echo "$filename"
		# echo "[$site] par $author le $date"
		# echo "$quote"
		# [ "$author" = "la r&#xE9;daction" ] && echo "hi"

		# echo "Année: $annee"
		mkdir -p "content/article/$annee"

		echo "---
site: $site
title: \"$title\"" > "$filename"
		[ "$author" != "la rédaction" ] && echo "author: $author" >> "$filename"
		echo "date: $date" >> "$filename"
		[ "$href" != '' ] && echo "href: $href" >> "$filename"
		echo "tags:$tags
---

$quote" >> "$filename"
	done
done

rm -- *.xml
