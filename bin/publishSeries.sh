#!/bin/bash

# Publish a series to april and linuxfr

# Requires "python3-html2text" for the function "html2markdown"

week=$(date -d '-4days' '+%Y%V')
shortWeek=$(date -d '-4days' '+%-V')
year=$(date -d '-4days' '+%Y')
previousWeek=$(date -d '-11days' '+%-V')
previousWeekYear=$(date -d '-11days' '+%Y')

echo "Publishing $week" | logger

# Go into working directory
[ -d "/tmp/fils/$week" ] || echo "/tmp/fils/$week doesn't exist yet, run mailSeries.sh first!"
cd "/tmp/fils/$week" || exit

title="Revue de presse de l’April pour la semaine $shortWeek de l’année $year"

body=$(hxprune -c bg-top "/tmp/fils/$week/index.html" |
	hxprune -c bg-black |
	hxprune -c bg-dark-gray |
	hxprune -c w-40-ns |
	hxprune -c readMore |
	hxprune -c flex-none |
	hxprune -c href |
	tidy -i -q -utf8)

# Only get the html "main" node content
body=${body#*<main class=?pb7? role=?main?>}
body=${body#*<div class=?series pa3 pa4-ns w-100 w-70-ns center?>}
body=${body%</main>*}
body=${body//w-60-ns }
body=${body%<section class=\"w-100 mw8\">*}
body=${body%</section>*}

# Récupération jeton drupal
getDrupalToken() {
	drupalToken=$(w3m -dump_source https://www.april.org/fr/node/add/actualite |
		grep form_token |
		grep edit-actualite-node-form-form-token |
		grep -o "value=\".*\"" |
		sed -n 's|value=\"\(.*\)\"|\1|p')

	[ -z "$drupalToken" ] && echo "Vous devez vous authentifier avec w3m sur https://www.april.org" && exit
}

publishDrupal() {
	getDrupalToken

	drupalHead=${body%%<section class=\"w-100 mw8\">*}
	drupalHead=$(echo "$drupalHead" | recode ..h)

	drupalBody=${body#*<section class=\"w-100 mw8\">}
	drupalBody=$(echo "$drupalBody" | recode ..h)

	drupalForm="<form id='myForm' action='https://www.april.org/node/add/actualite' method='post'>
	<h1>Drupal</h1>
	<input type='hidden' name='name' value='echarpentier' />
	<input type='hidden' name='form_id' value='actualite_node_form' />
	<input type='hidden' name='form_token' value=\"$drupalToken\" />

	<input type='hidden' name='taxonomy[1][]' value='2' />
	<input type='hidden' name='taxonomy[3][]' value='1586' />
	<input type='hidden' name='format' value='3' />
	<input type='hidden' name='teaser_include' value='1' />
	<input type='hidden' name='status' value='1' />
	<input type='hidden' name='promote' value='1' />
	<input type='hidden' name='pathauto_perform_alias' value='1' />

	<input name='title' size='80' value=\"$title\" />
	<br />
	<br />

	<h2>Head</h2>
	<textarea name='teaser_js' cols='80' rows='20'>$drupalHead</textarea>
	<h2>Body</h2>
	<textarea name='body' cols='80' rows='20'>$drupalBody</textarea>

	<br />
	<input type='submit' name='op' value='Enregistrer' />
	<input type='submit' name='op' value='Preview' />
	</form>"
	# <script>document.forms['myForm'].submit()</script>

	echo "$drupalForm" > drupal.html
	if [ "$(command -v firefox)" ]
	then
		firefox drupal.html
	else
		w3m drupal.html
	fi
}

# Récupération jeton linuxfr
getLinuxfrToken() {
	linuxfrToken=$(w3m -dump_source https://linuxfr.org/news/nouveau |
		grep authenticity_token |
		grep new_news |
		grep -o "name=\"authenticity_token\" value=\".*\"" |
		sed -n 's|name=\"authenticity_token\" value=\"\(.*\)\"|\1|p')
}

publishLinuxFr() {
	getLinuxfrToken
	if [ -z "$linuxfrToken" ]
	then
		echo "Problème avec la récupération de jeton linuxfr..." | logger
		exit
	fi

	markdown=$(echo "$body" | html2markdown --no-wrap-links -b 0)
	markdown=${markdown//  \*/*}

	linuxfrForm="<form id='myForm' action='https://linuxfr.org/news' method='post'>
	<h1>LinuxFr</h1>
	<input type='hidden' name='utf8' value='✓' />
	<input type='hidden' name='authenticity_token' value='$linuxfrToken' />
	<input type='hidden' name='news[section_id]' value='22' />
	<input type='hidden' name='news[cc_licensed]' value='1' />
	<input type='hidden' name='news[links_attributes][0][title]' value='April' />
	<input type='hidden' name='news[links_attributes][0][url]' value='https://www.april.org' />
	<input type='hidden' name='news[links_attributes][0][lang]' value='fr' />
	<input type='hidden' name='news[links_attributes][1][title]' value=\"Revue de presse de l'April\" />
	<input type='hidden' name='news[links_attributes][1][url]' value='https://www.april.org/revue-de-presse' />
	<input type='hidden' name='news[links_attributes][1][lang]' value='fr' />
	<input type='hidden' name='news[links_attributes][2][title]' value='Revue de presse de la semaine précédente' />
	<input type='hidden' name='news[links_attributes][2][url]' value='https://linuxfr.org/news/revue-de-presse-de-l-april-pour-la-semaine-$previousWeek-de-l-annee-$previousWeekYear' />
	<input type='hidden' name='news[links_attributes][2][lang]' value='fr' />
	<input type='hidden' name='news[links_attributes][3][title]' value='🕸 Fils du Net' />
	<input type='hidden' name='news[links_attributes][3][url]' value='https://filsdu.net' />
	<input type='hidden' name='news[links_attributes][3][lang]' value='fr' />
	<input type='hidden' name='news[urgent]' value='0' />
	<input type='hidden' name='tags' value='revue_de_presse' />
	<input type='hidden' name='news[message]' value='' />

	<input name='news[title]' size='80' value=\"$title\" />
	<br />
	<br />

	<h2>Head</h2>
	<textarea name='news[wiki_body]' cols='80' rows='20'>${markdown%%# *}</textarea>
	<h2>Body</h2>
	<textarea name='news[wiki_second_part]' cols='80' rows='20'>#  ${markdown#*# }</textarea>

	<br />
	<input type='submit' name='commit' value='Prévisualiser' />
	<input type='submit' name='commit' value='Soumettre cette dépêche' />
	</form>"
	# <script>document.forms['myForm'].submit()</script>

	echo "$linuxfrForm" > linuxfr.html
	if [ "$(command -v firefox)" ]
	then
		firefox linuxfr.html
	else
		w3m linuxfr.html
	fi
}

publishDrupal

publishLinuxFr
