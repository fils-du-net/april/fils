#!/bin/sh

# Useful in some themes like ananke
export HUGO_ENV="production"

/var/simpleWeb/bin/synchroGit.sh

cd /var/simpleWeb/apps/april.filsdu.net
sudo -u www-data HUGO_ENV="production" hugo -e production --gc --minify

cd /var/simpleWeb/apps/filsdu.net/content
git fetch --all
git merge --no-edit april/master

# Stops if already launched
[ -f /var/simpleWeb/run/filsdu.net.pid ] && exit

cd /var/simpleWeb/apps/filsdu.net
sudo -u www-data HUGO_ENV="production" hugo -e production --gc --minify
