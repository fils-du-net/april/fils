#!/bin/bash

# Import articles from an external april website
# You need to log in once with the command
# w3m "https://www.april.org/rp/"

week=$(date '+%V')
year=$(date '+%Y')

dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && cd .. && pwd)"

# Go into working directory
mkdir -p /tmp/fils && cd /tmp/fils || exit

# Download the URLs
wget "https://www.april.org/rp/?nid=1&cite=-1&order=0&q=&count=100"
mv index.html\?* index.html

mkdir -p "$dir/content/incoming/$year"
filename="$dir/content/incoming/$year/articles$week.txt"

data=$(grep -o "\\(href=\"https://[^\"]*\\|<td>IRC(.*)<\\/td>\\)" index.html |
	grep -o "\\(https://[^\"]*\\|IRC(.*)\\)")

if [ -z "$data" ]
then
	echo "getAprilArticles: No incoming url" | logger
	exit
fi

echo "Incoming articles stored in $filename" | logger
echo "$data" >> "$filename"

urls=$(echo "$data" | grep -o "https://[^\"]*")

for url in $urls
do
	# shellcheck source=url2fils.sh
	source "$dir/bin/url2fils.sh" "$url"
done

toRemove=$(grep -o "enlever([[:digit:]]*)" index.html | grep -o "[[:digit:]]*")
for i in $toRemove
do
	wget "https://www.april.org/rp/enlever_article.php?id=$i"
	rm enlever_article.php\?id=*
done
