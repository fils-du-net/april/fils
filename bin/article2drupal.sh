#!/bin/bash

article="$1"

echo "Exporting $article to drupal" | logger

extract() {
	# This is the destination variable
	declare -n field=$1

	field=$(grep -o "$1: .*" "$article")
	field=${field#* }
	field=${field#\"}
	field=${field%\"}

	echo "$1: $field"
}

# Declaring variables that extractions will output to
site=
title=
author=
date=
href=
abstract=
series_weight=

extract site
extract title
extract author
extract date
extract href
extract series_weight
[ "$series_weight" = "0" ] && series_weight=1
tags=$(grep -o "\\- .*" "$article")
echo "tags: $tags"
abstract=$(grep -o "> .*" "$article")
abstract=${abstract#> }
echo "abstract: $abstract"

# Récupération jeton drupal
getDrupalToken() {
	drupalToken=$(w3m -dump_source http://www.april.org/fr/node/add/revue-de-presse|
		grep form_token|
		grep edit-revue-de-presse-node-form-form-token|
		grep -o "value=\".*\"" |
		sed -n 's|value=\"\(.*\)\"|\1|p')

	[ -z "$drupalToken" ] && echo "Vous devez vous authentifier avec w3m sur https://www.april.org" && exit
}

publishDrupal() {
	getDrupalToken

	drupalForm="<form id='myForm' action='https://www.april.org/fr/node/add/revue-de-presse' method='post'>
	<input type='hidden' name='name' value='echarpentier' />
	<input type='hidden' name='form_id' value='revue_de_presse_node_form' />
	<input type='hidden' name='form_token' value=\"$drupalToken\" />

	<input type='hidden' name='taxonomy[3][]' value='1586' />
	<input type='hidden' name='status' value='1' />
	<input type='hidden' name='format' value='1' />
	<input type='hidden' name='teaser_include' value='1' />
	<input type='hidden' name='pathauto_perform_alias' value='1' />

	<input name='field_nom_du_site[0][value]' value=\"$site\" />
	<input name='title' value=\"[$site] $title\" />
	<input name='field_auteur_article[0][value]' value=\"$author\" />
	<input name='field_date[0][value]' value=\"$(date '+%A %-d %B %Y' -d $date)\" />
	<input name='field_date_sort[0][value][date]' value=\"$date\" />
	<input name='field_url_article[0][url]' value=\"$href\" />
	<input name='field_mots_cle[0][value]' value=\"$series_weight\" />

	<textarea name='body'>${abstract}</textarea>
	<textarea name='field_article_complet[0][value]'>$(w3m -dump "$href")</textarea>

	<select name='taxonomy[10][]' multiple='multiple'>
	<option value='1657'>Le Logiciel Libre</option>
	<option value='1669'>Entreprise</option>
	<option value='1682'>Internet</option>
	<option value='1683'>Logiciels privateurs</option>
	<option value='1660'>Administration</option>
	<option value='1678'>Économie</option>
	<option value='1659'>Interopérabilité</option>
	<option value='1665'>april</option>
	<option value='1685'>Partage du savoir</option>
	<option value='1680'>HADOPI</option>
	<option value='1889'>Matériel libre</option>
	<option value='1658'>Institutions</option>
	<option value='1671'>Sensibilisation</option>
	<option value='1666'>Vente liée</option>
	<option value='1674'>Accessibilité</option>
	<option value='1675'>Associations</option>
	<option value='1661'>Brevets logiciels</option>
	<option value='1667'>DADVSI</option>
	<option value='1677'>Désinformation</option>
	<option value='1662'>DRM</option>
	<option value='1668'>Droit d'auteur</option>
	<option value='1663'>Éducation</option>
	<option value='1664'>Informatique-deloyale</option>
	<option value='1681'>Innovation</option>
	<option value='1670'>Licenses</option>
	<option value='1848'>Marchés publics</option>
	<option value='1684'>Neutralité du Net</option>
	<option value='1673'>Philosophie GNU</option>
	<option value='1686'>Promotion</option>
	<option value='1698'>RGI</option>
	<option value='1687'>Sciences</option>
	<option value='1672'>Standards</option>
	<option value='1691'>Vote électronique</option>
	<option value='1853'>Informatique en nuage</option>
	<option value='1849'>Vidéo</option>
	<option value='1714'>Contenus libres</option>
	<option value='1679'>Europe</option>
	<option value='1708'>International</option>
	<option value='1701'>ACTA</option>
	<option value='1700'>English</option>
	<option value='1890'>Open Data</option>
	<option value='1898'>Vie privée</option>
	</select>

	<input type='submit' name='op' value='Enregistrer' />
	<input type='submit' name='op' value='Aperçu' />
	</form>
	<script>document.forms['myForm'].submit()</script>"

	while read -r i
	do
		# echo "tag: $i"
		drupalForm=${drupalForm/>$i/ selected=\'selected\'>$i}
		# echo "$drupalForm"|grep select
	done <<< "${tags//- }"

	echo "$drupalForm" | w3m -T text/html
	# echo "$drupalForm" > drupal.html
	# firefox drupal.html
}

publishDrupal
